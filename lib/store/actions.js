import history from '../history';
import toNative from '../js-native-bridge/toNative';

import {obs_ROUTE_CHANGED, obs_NEW_DATA_RECEIVED} from './constants';

export const action = {

  // Request JsonData from native
  getStoreData: () => {
    toNative.getStoreData();
  },

  // response with JsonData from native
  setStoreData: (jsonArray) => {
    for (let i = 0; i < jsonArray.length; i++) {

      const objKey = Object.keys(jsonArray[i])[0];

      // set Data to Store
      window.store.props[objKey] = jsonArray[i][objKey];
    }


    window.store.trigger(obs_NEW_DATA_RECEIVED);
  },

  // Change Route -> also to be called from Native
  changeRoute: (newRoute) => {
    window.store.props.currentRoute = newRoute;
    history.push(newRoute);
  },

  routeChanged: () => {
    window.store.trigger(obs_ROUTE_CHANGED);
  },

  // send a debugMessage to native
  sendDebugMessageToNative: (payload) => {
    toNative.sendDebugMessageToNative(payload);
  }

}
 
