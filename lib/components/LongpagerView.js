import React, {PureComponent} from 'react';
import {ArticleBuilder} from 'modulelib/build';
import {connect} from 'react-redux';

class LongpagerView extends PureComponent {

  param = null;
  longpager = undefined;

  constructor(props) {
    super(props);
    this.param = this.props.match.params;

    //send request for longpager to JsNative
    if(this.param !== null) {
      window.jsNativeBridge.sendMessage('REQUEST_LONGPAGER',
        {
          id: this.param.id
        });
    }
  }

  componentWillMount() {
    this.longpager = window.store.props.data.longpager;
    this.setState({
      rec_json: false
    });
    if(this.longpager !== undefined) {
      this.setState({
        rec_json: true
      });
    }
  }

  componentDidMount() {

  }

  render() {

    const data = this.props;

    return(
      <div>
        {/*{this.state.rec_json &&*/}
        {/*<ArticleBuilder data={this.longpager}/>*/}
        {/*}*/}

      </div>
    );
  }
}

function mapStateToProp(state) {
  return state;
}

export default connect(mapStateToProp)(LongpagerView);
