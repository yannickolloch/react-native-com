export const routes = {
  '/': 'start',
  '/models': 'models',
  '/longpager': 'longpager',
  '/models/mod-1': 'mod_1',
  '/models/mod-2': 'mod_2',
  '/models/mod-3': 'mod_3',
  '/models/mod-4': 'mod_4',
  '/models/mod-5': 'mod_5',
  '/accessories': 'accessories',
  '/accessories/mod-3-mod-4-mod-x3': 'mod_3_mod_4_mod_x3',
  '/accessories/m-performance-parts': 'm_performance_parts'
};

export const longpager_json = {
  'market': 'DIN',
  'language': 'de',
  'collection': 'test',
  'link': 'AO_Innovation_Technology_Safety_G30',
  'title': 'AO_Innovation_Technology_Safety_G30 (DIN)',
  'rightToLeft': false,
  'modules': [{
    'name': 'm02_overview-header',
    'config': {
      'headline': 'DER NEUE BMW 5er TOURING.',
      'background': {
        'url': 'http://via.placeholder.com/600x300'
      },
      'type': '',
      'subline': 'ERLEBEN SIE DEN NEUEN BMW 5er.<sup>1</sup>',
      'copy': 'Herausragend. Innovativ.',
      'url': './bla'
    }
  }, {
    'name': 'm26_small-media-link',
    'config': {
      'text': 'Teilen Sie Ihre Begeisterung für den BMW 5er Touring.',
      'url': '#'
    }
  }, {
    'name': 'm03_overview-cardslider',
    'config': {
      'headline': 'SPECIALS UND SERVICES.',
      'teasers': [{
        'headline': 'SPECIALS UND SERVICES',
        'style': '',
        'url': '#',
        'image': {
          'url': 'http://via.placeholder.com/600x300'
        }
      }, {
        'headline': 'BMW PARTNER FINDEN.',
        'style': '',
        'url': '#',
        'image': {
          'url': 'http://via.placeholder.com/600x300'
        }
      }]
    }
  }, {
    'name': 'm42_rate',
    'config': {
      'headline': 'IHRE MEINUNG ZÄHLT.',
      'subline1': 'WIE GEFÄLLT IHNEN DIE APP?',
      'subline2': 'MÖCHTEN SIE UNS AUCH IM APP STORE BEWERTEN?',
      'subline3': 'VIELEN DANK FÜR IHRE BEWERTUNG.',
      'image': 'http://via.placeholder.com/600x300',
      'placeholder': 'Bitte tippen Sie hier gewünschte Funktionen ein',
      'feedbackTitle': 'EINE FRAGE ZUR APP:',
      'feedbackText': 'Welche der folgenden Punkte treffen auf Ihre Erfahrung mit der BMW Kataloge App zu?',
      'feedback1': 'Mir haben Funktionalitäten gefehlt.',
      'feedback2': 'Die Inhalte waren für mich irrelevant.',
      'feedback3': 'Die App-Navigation war irreführend.',
      'feedback4': 'Mir hat das Design nicht gefallen.',
      'feedback5': 'Die App ist abgestürzt.',
      'feedback6': 'Sonstiges (bitte erläutern):',
      'btnRate': 'App bewerten',
      'btnYes': 'Ja',
      'btnNo': 'Nein',
      'btnFeedback': 'Senden'
    }
  },{
    'name': 'm07_header',
    'config': {
      'headline': 'Empfohlene Themen',
      'isFixed': false,
      'teasers': [
        {
          'style': 'back col-xs-6',
          'image': {
            'url': 'http://placehold.it/442x273/666/fff'
          },
          'topline': 'Topline',
          'headline': 'Teaser Card.',
          'url': '#'
        },
        {
          'style': 'col-xs-6',
          'image': {
            'url': 'http://placehold.it/442x273/666/fff'
          },
          'topline': 'Topline',
          'headline': 'Teaser Card.',
          'url': '#'
        }
      ]
    }
  }]
};

export const data = {
  mock: true,
  longpager: {}
};
