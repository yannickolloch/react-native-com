import {data, routes} from '../dummy_JSON';

export const ARTICLE1_UUID = 'a41ba380-69b9-41ac-bf58-1843a941329e';
export const ARTICLE2_UUID = '811f8fdf-c7f7-43db-8823-b106f4995c88';

let article1 = {
  'market': 'DIN',
  'language': 'de',
  'collection': 'test',
  'link': 'AO_Innovation_Technology_Safety_G30',
  'title': 'AO_Innovation_Technology_Safety_G30 (DIN)',
  'rightToLeft': false,
  'modules': [{
    'name': 'm02_overview-header',
    'config': {
      'headline': 'DER NEUE BMW 5er TOURING.',
      'background': {
        'url': 'http://via.placeholder.com/600x300'
      },
      'type': '',
      'subline': 'ERLEBEN SIE DEN NEUEN BMW 5er.<sup>1</sup>',
      'copy': 'Herausragend. Innovativ.',
      'url': './bla'
    }
  }, {
    'name': 'm26_small-media-link',
    'config': {
      'text': 'Teilen Sie Ihre Begeisterung für den BMW 5er Touring.',
      'url': '#'
    }
  }, {
    'name': 'm03_overview-cardslider',
    'config': {
      'headline': 'SPECIALS UND SERVICES.',
      'teasers': [{
        'headline': 'SPECIALS UND SERVICES',
        'style': '',
        'url': '#',
        'image': {
          'url': 'http://via.placeholder.com/600x300'
        }
      }, {
        'headline': 'BMW PARTNER FINDEN.',
        'style': '',
        'url': '#',
        'image': {
          'url': 'http://via.placeholder.com/600x300'
        }
      }]
    }
  }, {
    'name': 'm42_rate',
    'config': {
      'headline': 'IHRE MEINUNG ZÄHLT.',
      'subline1': 'WIE GEFÄLLT IHNEN DIE APP?',
      'subline2': 'MÖCHTEN SIE UNS AUCH IM APP STORE BEWERTEN?',
      'subline3': 'VIELEN DANK FÜR IHRE BEWERTUNG.',
      'image': 'http://via.placeholder.com/600x300',
      'placeholder': 'Bitte tippen Sie hier gewünschte Funktionen ein',
      'feedbackTitle': 'EINE FRAGE ZUR APP:',
      'feedbackText': 'Welche der folgenden Punkte treffen auf Ihre Erfahrung mit der BMW Kataloge App zu?',
      'feedback1': 'Mir haben Funktionalitäten gefehlt.',
      'feedback2': 'Die Inhalte waren für mich irrelevant.',
      'feedback3': 'Die App-Navigation war irreführend.',
      'feedback4': 'Mir hat das Design nicht gefallen.',
      'feedback5': 'Die App ist abgestürzt.',
      'feedback6': 'Sonstiges (bitte erläutern):',
      'btnRate': 'App bewerten',
      'btnYes': 'Ja',
      'btnNo': 'Nein',
      'btnFeedback': 'Senden'
    }
  }, {
    'name': 'm07_header',
    'config': {
      'headline': 'Empfohlene Themen',
      'isFixed': false,
      'teasers': [
        {
          'style': 'back col-xs-6',
          'image': {
            'url': 'http://placehold.it/442x273/666/fff'
          },
          'topline': 'Topline',
          'headline': 'Teaser Card.',
          'url': '#'
        },
        {
          'style': 'col-xs-6',
          'image': {
            'url': 'http://placehold.it/442x273/666/fff'
          },
          'topline': 'Topline',
          'headline': 'Teaser Card.',
          'url': '#'
        }
      ]
    }
  }]
};

let article2 = {
  'market': 'DIN',
  'language': 'de',
  'collection': 'test',
  'link': 'AO_Innovation_Technology_Safety_G30',
  'title': 'AO_Innovation_Technology_Safety_G30 (EN)',
  'rightToLeft': false,
  'modules': [{
    'name': 'm07_header',
    'config': {
      'headline': 'SICHERHEIT.',
      'headlinelevel': 'h1',
      'subheadline': 'OPTIMALES SCHUTZ DURCH NEUESTE BMW TECHNOLOGIEN.',
      'subheadlinelevel': 'h2',
      'introcopy': 'In der neuen BMW 5er Limousine erleben Sie hochmoderne Technologien, auf die immer Verlass ist. Überlegene Fahreigenschaften und innovative Ausstattungen gehören hier zum Standard. Als BMW Fahrer verfügen Sie über fortschrittlichste Technologien, die Sie in jeder Situation unterstützen. So können Sie sich bei allen Straßenverhältnissen sicher fühlen.',
      'introcopyIcon': false,
      'slider': {
        'modules': [{
          'name': 'e02_image',
          'config': {
            'url': 'http://via.placeholder.com/600x300'
          }
        }]
      }
    }
  }, {
    'name': 'm09_teaser',
    'config': {
      'isFixed': true,
      'teasers': [
        {
          'style': 'back col-xs-6',
          'image': {
            'url': 'http://via.placeholder.com/600x300'
          },
          'topline': 'INNOVATION UND TECHNIK.',
          'headline': 'FAHRWERK.',
          'url': '#'
        },
        {
          'style': 'col-xs-6',
          'image': {
            'url': 'http://via.placeholder.com/600x300'
          },
          'topline': 'INNOVATION UND TECHNIK.',
          'headline': 'INNOVATION UND TECHNIK.',
          'url': '#'
        }
      ]
    }
  }]
};

export function getMockLongpager(message) {
  const jsonArray = {
    payload: {
      routes: routes,
      data: data
    }
  };
  if (message.payload.id === ARTICLE1_UUID) {
    jsonArray.payload.data.longpager = article1;
    return jsonArray;
  } else if (message.payload.id === ARTICLE2_UUID) {
    jsonArray.payload.data.longpager = article2;
    return jsonArray;
  }
}