import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';
import findGetParameter from '../utils/findGetParameter';
import JsNativeBridge from '../js-native-bridge/JsNativeBridge';
import {data, routes, longpager_json} from '../dummy_JSON';
import {HashRouter, Route, Switch} from 'react-router-dom';
import LongpagerView from '../components/LongpagerView';
import confStore from '../redux/store';
import {Provider} from 'react-redux';
import actions from '../redux/actions';


let initState = {
  currentRoute: '/',
  routes: null,
  data: null
};


let host = window.host;

const getParamHost = findGetParameter(host);
if (getParamHost) {
  host = getParamHost;
}

let store = confStore(initState);
window.jsNativeBridge = new JsNativeBridge(host, store);


const jsonArray = {
  routes: routes,
  data: data
};

store.dispatch(actions.setStoreData(jsonArray));

window.jsNativeBridge.receiveMessage();

// mock from native
jsonArray.data.longpager = longpager_json;
window.jsNativeBridge.sendMessage('SET_STORE_DATA', jsonArray);
//////////////////////////////////////////////////////////////

let routess = (
  <HashRouter>
    <Switch>
      <Route exact path={'/'} component={App}/>
      <Route path={'/article/:id'} component={LongpagerView}/>
    </Switch>
  </HashRouter>
);

ReactDOM.render(
  <Provider store={store}>
    <HashRouter>{routess}</HashRouter></Provider>,
  document.getElementById('root')
);
