import {createStore, applyMiddleware, compose} from 'redux';
import reducer from './reducer';
import {createLogger} from 'redux-logger';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let finalCreateStore = composeEnhancers(
  applyMiddleware(createLogger())
)(createStore);

export default function configureStore(initialState) {
  return finalCreateStore(reducer, initialState);
}