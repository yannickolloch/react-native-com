import {obs_NEW_DATA_RECEIVED, obs_ROUTE_CHANGED} from './constants';

export default function reducer(state, action) {

  switch (action.type) {
  case obs_NEW_DATA_RECEIVED:
    return Object.assign({}, state, {
      data: action.data.data,
      routes: action.data.routes
    });
  case obs_ROUTE_CHANGED:
    Object.assign({}, state);
    break;
  default:
    return state;
  }
}