import history from '../history';
import toNative from '../js-native-bridge/toNative';
import {obs_NEW_DATA_RECEIVED, obs_ROUTE_CHANGED} from '../store/constants';

let actions = {

  // Request JsonData from native
  getStoreData: () => {
    toNative.getStoreData();
  },

  // response with JsonData from native
  setStoreData: (data) => {
    console.log(data);
    return {
      type: obs_NEW_DATA_RECEIVED,
      data: data
    };
  },

  // Change Route -> also to be called from Native
  changeRoute: (newRoute) => {
    window.store.props.currentRoute = newRoute;
    history.push(newRoute);
  },

  routeChanged: () => {
    window.store.trigger(obs_ROUTE_CHANGED);
  },

  // send a debugMessage to native
  sendDebugMessageToNative: (payload) => {
    toNative.sendDebugMessageToNative(payload);
  }
};

export default actions;