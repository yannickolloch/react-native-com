import actions from '../redux/actions';
import _ from 'lodash';

export default {

  // Response from getStoreData (in toNative.js) from Native
  setStoreData: (store, jsonArray) => {
    store.dispatch(actions.setStoreData(_.cloneDeep(jsonArray)));
  },

  // Change Route call from Native
  changeRoute: (store, newRoute) => {
    store.dispatch(actions.setStoreData(_.cloneDeep(newRoute)));
  }
};