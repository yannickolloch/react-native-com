// actions send to native
export default {

  // Request JsonData asyncronous (responseHandler is in fromNative.js)
  getStoreData: () => {
    window.jsNativeBridge.sendMessage('GET_STORE_DATA', null);
  },

  // send a debugMessage to native
  sendDebugMessageToNative: (payload) => {
    window.jsNativeBridge.sendMessage("DEBUG_MESSAGE_FROM_JS", payload);
  }
}