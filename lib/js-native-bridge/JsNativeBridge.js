import fromNative from './fromNative';
import {getMockLongpager} from '../mock/Mock';

// Bridge is initialized in index.js
export default class JsNativeBridge {

  mock = true;

  constructor(host, store) {
    // IOS / ANDROID - needed because the call to native is plattform specific
    this.host = host;
    this.store = store;
  }

  // call to native
  sendMessage(type, payload) {
    let message = {
      type: type,
      payload: payload
    };
    if (this.mock) {
      switch (type) {
      case 'REQUEST_LONGPAGER':
        window.dispatchEvent(new CustomEvent('receiveMessage', {detail: getMockLongpager(message)}));
        break;
      default:
        window.dispatchEvent(new CustomEvent('receiveMessage', {detail: message}));
      }
    } else {
      // Plattform dependent call to native
      switch (this.host) {
      case 'IOS':
        window.webkit.messageHandlers.jsAppMessageReceiver.postMessage(message); // injected global to window from IOS
        break;
      case 'ANDROID':
        window.NativeJsBridge.postMessage(message); // injected global to window from Android -> on Android not testet yet
        break;
      default:
        console.dir(message, {depth: null});
      }
    }
  }

  // called from native: window.jsNativeBridge.receiveMessage('TYPE_STRING', 'payloadString')
  receiveMessage(type, payload) {
    if (this.mock === true) {
      window.addEventListener('receiveMessage', (e) => {
        fromNative.setStoreData(this.store, e.detail.payload);
      });
    } else {
      switch (type) {
      case 'SET_STORE_DATA':
        fromNative.setStoreData(this.store, payload);
        break;
      case 'CHANGE_ROUTE':
        fromNative.changeRoute(this.store, payload);
        break;
      default:
        this.sendMessage('DEBUG_MESSAGE_FROM_JS', '=> JsNativeBridge.receivedMessage - sent message could not be handled -> wrong \'type\'?');
      }
    }
  }
}