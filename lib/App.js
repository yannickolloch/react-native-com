import React, {Component} from 'react';
import {connect} from 'react-redux';
import {ArticleBuilder} from 'modulelib/build';
import {ARTICLE1_UUID, ARTICLE2_UUID} from './mock/Mock';

class App extends Component {

  handleClick = (id, event) => {
    window.jsNativeBridge.sendMessage('REQUEST_LONGPAGER',
      {
        id: id
      });
  };

  render() {
    return (
      <div className={'container'}>
        <button onClick={(e) => this.handleClick(ARTICLE1_UUID, e)}>Article1</button>
        <button onClick={(e) => this.handleClick(ARTICLE2_UUID, e)}>Article2</button>
        <p>{this.props.data.longpager.title}</p>
        <ArticleBuilder data={this.props.data.longpager}/>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  data: state.data
});

export default connect(mapStateToProps)(App);
